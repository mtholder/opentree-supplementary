#!/usr/bin/env python
import sys
from classify_names import triple_filename_to_name2obj
if __name__ == '__main__':
    om, omax_id, oroot = triple_filename_to_name2obj(sys.argv[1], True)
    nm, nmax_id, nroot = triple_filename_to_name2obj(sys.argv[2], True)
    print 'first taxonomy'
    for nd in oroot.child_list:
        print '   ', nd.original_name
        for nd2 in nd.child_list:
            print '       ', nd2.original_name
    print 'second taxonomy'
    for nd in nroot.child_list:
        print '   ', nd.original_name
        for nd2 in nd.child_list:
            print '       ', nd2.original_name
