Poorly documented scripts, programs and output developed in the process of 
testing and designing opentree software. This is intended for software that
will never become part of the production code.

TreeBase names
==============
MTH used wget to pull down 3,421 studies from TreeBase on Aug 24-Aug 25, 2012.
The NEXUS files had names like S###.nex where ### was a number between 0 and 
14,000.  Using a script in the repo and an invocation like this:

    range_of_treebase2newick.sh  0 14000

he was able to create 3,071 newick trees files from the first TREES block of 
most of the NEXUS files. That script relies on the example NCLconverter 
executable that is a part of the NEXUS class library, v2.1 which is available 
via:

    svn co https://ncl.svn.sourceforge.net/svnroot/ncl/branches/v2.1

Note that if there are multiple TREES blocks in a file, the NCLconverter will
produce different .tre files with a number (2 for the second block, 3 for the 
third...) as the first part of the name. To get just the trees from the first 
block by selecting the files that did not have a number. Specifically he used:

    export TBDIR=/tmp/treebase_newick_2012_08_25
    mkdir ${TBDIR}
    find . -name "out*x.tre"  -exec cp {} ${TBDIR}/ \;

to move those files to a new directory.  To extract just the OTU names from the
newick trees. A couple of manual adjustments were made (because NCL does not 
correctly quote taxon labels with single quotes in the name when it is exporting
to newick format):

    g++ newick2taxalist.cpp -o newick2taxalist -I "${NCL_ROOT}/include" -lncl -L "${NCL_ROOT}/lib/ncl"
    ls ${TBDIR}/*tre > inp_list.txt
    ./newick2taxalist -frelaxedphyliptree  -linp_list.txt  > unsorted.txt
    sort unsorted.txt | uniq > ${TBDIR}/taxa_sorted_uniq.txt
    
    
The resulting treebase_newick_2012_08_25 directory was tar'd, gzipped and posted
as [treebase_newick_2012_08_25.tar.gz](https://bitbucket.org/mtholder/opentree-supplementary/downloads/treebase_newick_2012_08_25.tar.gz)

[TreeBase](http://treebase.org/) study ID's have been retained as part of each
tree's filename so that the full provenance info can be retrieved as needed.

To prepare for adding new taxonomic names (to my local installation of the 
treemachine). I run the following script to classify the names that are not 
found in the current version of OTToL, you'll first need to grab a 
compact form of the OTToL taxonomy from [Stephen's bitbucket download area](https://bitbucket.org/blackrim/avatol-taxonomies/downloads), 
and then do something like this:

    python classify_names.py /tmp/OTToL061912.txt  ${TBDIR}/taxa_sorted_uniq.txt  > taxon_postmortem.txt
    

