#!/usr/bin/env python
import sys, os
from dendropy import Tree
from classify_names import Taxonomy, HomonymError

def set_of_ancestors(x):
    s = set()
    try:
        for el in x:
            s.update(el.ancestors())
    except:
        s.update(x.ancestors())
    return s

def choose_homonym(anc_set, match_list):
    step_match_list = []
    for match in match_list:
        a_list = match.ancestors()
        for n, i in enumerate(a_list):
            if i in anc_set:
                num_shared = len(a_list) - n - 1
                step_match_list.append((num_shared, match))
                break
    step_match_list.sort()
    longest_shared = step_match_list[-1]
    taxon = longest_shared[-1] 
    sys.stderr.write('#homonym resolved to %s (%d)\n' % (taxon.canonical_name, taxon.ottol_id))
    return taxon

#Args:
# Taxonomy-triples input-dir new-names-to-taxonomy
taxonomy = Taxonomy(sys.argv[1])

in_dir = sys.argv[2]
inp = os.listdir(in_dir)


new_taxa_file = open(sys.argv[3], 'w')
for fn in inp:
    if not fn.endswith('.tre'):
        continue
    inp = open(os.path.join(in_dir, fn), 'rU')
    
    ofn = os.path.join('outp', os.path.split(fn)[-1])
    outp = None 
    
    for tree_ind, line in enumerate(inp):
        try:
            tree = Tree.get_from_string(line, 'newick')
        except:
            sys.stderr.write('#dendropy could not read  %d of %s \n' % (1 + tree_ind, fn)) 
            continue
        homonym_list = []
        matched_list = []
        for tax in  tree.taxon_set:
            try:
                name_obj, added = taxonomy.classify_name(tax.label, True, new_taxa_file)
                matched_list.append((tax, name_obj))
            except HomonymError, x:
                homonym_list.append((tax, x.matches))
        disconnected = []
        connected = []
        for tax, name_obj in matched_list:
            nr = name_obj.find_root()
            if nr == taxonomy.root:
                connected.append((tax, name_obj))
            else:
                disconnected.append((tax, name_obj))
        ancestors_set = set_of_ancestors([n for t, n in connected])
        for tax, matches in homonym_list:
            try:
                name_obj = choose_homonym(ancestors_set, matches)
                nr = name_obj.find_root()
                if nr == taxonomy.root:
                    ancestors_set.update(name_obj.ancestors())
                    connected.append((tax, name_obj))
                else:
                    disconnected.append((tax, name_obj))
            except:
                sys.stderr.write('#homonym "%s" of tree %d of %s could not be resolved\n' % (tax.label, 1 + tree_ind, fn))
        if len(connected) > 2:
            to_prune = [d for d, name_obj in disconnected]
            tree.prune_taxa(to_prune)
            sys.stderr.write('#%d leaves pruned from tree %d of %s\n' % (len(disconnected),1 + tree_ind, fn))
            if outp is None:
                outp = open(ofn, 'w')
            outp.write('%s\n' % tree.as_string('newick', suppress_rooting=True))
        else:
            sys.stderr.write('#tree %d of %s could not be matched to taxonomy\n' % (1 + tree_ind, fn))
        
    
