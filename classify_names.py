#!/usr/bin/env python
import sys, re
import logging
arg_list = sys.argv
logging.basicConfig()
_LOG = logging.getLogger('classify_names')
if '-d' in arg_list:
    _LOG.setLevel(logging.DEBUG)
    d_ind = arg_list.index('-d')
    arg_list = arg_list[:d_ind] + arg_list[d_ind + 1:]
else:
    _LOG.setLevel(logging.INFO)
def taxon_file_to_dict(fo):
    #r = set()
    r = {}
    for line in fo:
        ls = line.strip().split(',')
        if len(ls) == 3:
            name = ls[2]
        else:
            name = ','.join(ls[2:])
        try:
            tid, pid, = int(ls[0]), int(ls[1])
        except:
            if ls[1]:
                sys.exit(line)
                raise
            tid = int(ls[0])
            pid = -1
        #r.add(name)
        #if name.strip().startswith('Abelmoschus ficu'):
        #    sys.exit(repr(name))
        r[(name, tid)] = (tid, pid)
        prev_list = r.get(name)
        if prev_list:
            prev_list.append(tid)
        else:
            r[name] = [tid]
    return r

class TR(object):
    '''
    Taxonomic Rank
    '''
    UNKNOWN, GENUS, SPECIES, SUBSPECIES, VARIETY, INDIVIDUAL, HAPLOTYPE = range(7)

class N(object):
    '''
    Name
    '''
    str_list = ['POSSIBLE_TAXON', 
                'EX_TAG',
                'VAR_TAG',
                'SSP_TAG',
                'SP_TAG',
                'AFF_TAG',
                'DB_IDENTIFIER',
                'LAB_CODE',
                'PERPLEXING',
                'POSSIBLE_EPITHET',
                'NONE',
                'POORLY_FORMED',          # significant syntax codes found, but in an unexpected contex (e.g. name ends with 'aff')
                'AFFINITY_TO_SPECIES',
                'AFFINITY_TO_SUBSPECIES',
                'GENUS_SP',
                'GENUS_SP_NOV',
                'BINOMEN',
                'TRINOMEN',
                'NUMBER',
                'UNKNOWN_TAG',
                'VARIETY_OF',
                'PREPROCESSED'
                ]
    number_pat = re.compile(r'^\d+$')
    accession_pat = re.compile(r'^[A-Z]{1,4}\d{4,10}$')
    nominal = re.compile(r'^[-a-zA-Z]+$')
    first_cap = re.compile(r'^[A-Z][a-z]+$')
    novum_id = re.compile(r'^nov([\d]+)')
    epithet_with_suffix = re.compile(r'^([-a-z]{2,80})([A-Z0-9]\S*)$')

    def find_root(self):
        p = self.par
        if p is None:
            return self
        return p.find_root()
    
    def ancestors(self):
        r = []
        p = self
        while p is not None:
            r.append(p)
            p = p.par
        return r
        
    
    def get_attached_lacking_id(self):
        r = []
        if self.ottol_id is not None:
            return r
        r.append(self)
        if self.par is not None:
            r.extend(self.par.get_attached_lacking_id())
        if isinstance(self.sub, list):
            for el in self.sub:
                r.extend(el.get_attached_lacking_id())
        return r
    def code2str(c):
        return N.str_list[c]
    code2str = staticmethod(code2str)
    
    def __init__(self,
                 name,
                 name_type,
                 par=None,
                 rank=None,
                 sub=None,
                 entity_id_list=None, # ID suffix
                 id_type=None,
                 canonical=None,
                 ottol_id=None):
        self.ottol_id = ottol_id
        assert isinstance(name, str)
        self.original_name = name
        if canonical is None:
            self.canonical_name = name
        else:
            self.canonical_name = canonical 
        self.name_diffs = None #
        self.enum = name_type
        self.par = par
        self.rank = rank
        self.sub = sub
        self.identifier = entity_id_list
        self.id_type = id_type
        if self.enum == N.EX_TAG:
            # the first sub item is the entity, the second is the "host"
            assert(len(sub) == 2)
            self.par = self.sub[0].par
            self.rank = self.sub[0].rank
    def has_taxonomic_name(self):
        if self.rank is None or self.rank == TR.UNKNOWN:
            if self.enum in [N.EX_TAG, N.VAR_TAG, N.SSP_TAG, N.SP_TAG, N.AFF_TAG, 
                             N.DB_IDENTIFIER, N.LAB_CODE, N.PERPLEXING,
                             N.POSSIBLE_EPITHET, N.NONE, N.POORLY_FORMED, 
                             N.NUMBER, N.UNKNOWN_TAG]:
                return False
        return True
    def flag_as_poorly_formed(self):
        self.enum = N.POORLY_FORMED
    def add_name_diff(self, d):
        if self.name_diffs is None:
            self.name_diffs = []
        self.name_diffs.append(d)
    def is_species(self):
        if self.enum == N.EX_TAG:
            return self.sub[0].is_species()
        if self.enum == N.VARIETY_OF:
            return False
        return self.rank == TR.SPECIES

    def is_subspecies(self):
        if self.enum == N.EX_TAG:
            return self.sub[0].is_subspecies()
        if self.enum == N.VARIETY_OF:
            return TRUE
        return self.rank == TR.SUBSPECIES

    def diagnostic_str(self):
        if self.enum == N.EX_TAG:
            return '%s_EX_%s: %s' % (N.code2str(self.sub[0].enum), 
                                     N.code2str(self.sub[1].enum),
                                     self.canonical_name)
        if self.enum == N.VARIETY_OF:
            if self.par.is_species():
                t = 'VARIETY_OF_SP_'
            elif self.par.is_subspecies():
                t = 'VARIETY_OF_SUBSP_'
            else:
                t = 'VARIETY_OF_TAXON_'
            core= '%s%s' % (t, N.code2str(self.par.enum))
        else:
            core = N.code2str(self.enum)
        
        if self.id_type is not None:
            st = self.get_suffix_type_str()
            core = "%s_SUFFIX_%s" % (core, st)
        return "%s: %s" % (core, self.canonical_name)
    def get_suffix_type_str(self):
        if not self.id_type:
            return 'NONE'
        return '_'.join([N.code2str(i) for i in self.id_type])
        
for code, word in enumerate(N.str_list):
    setattr(N, word, code)
    
def _process_trailing(name_map, word_list, ind):
    '''
    Called after all elements of a binomen or trinomen have been read.
    '''
    codes = []
    suffix_list = word_list[ind:]
    for word in suffix_list:
        if N.accession_pat.match(word):
            c = N.DB_IDENTIFIER
        else:
            c = N.LAB_CODE
        codes.append(c)
    
    return suffix_list, codes
    

def classify_word_type(word):
    if N.number_pat.match(word):
        return N.NUMBER
    lw = word.lower()
    if lw in ['sp', 'sp.', 'Sp', 'Sp.' ]:
        return N.SP_TAG
    if lw in ['ssp', 'ssp.', 'subsp']:
        return N.SSP_TAG
    if lw in ['aff', 'aff.']:
        return N.AFF_TAG
    if lw in ['var', 'var.']:
        return N.VAR_TAG
    if lw in ['ex', 'ex.', 'from']:
        return N.EX_TAG
    if N.accession_pat.match(word):
        return N.DB_IDENTIFIER
    if N.nominal.match(word):
        if lw == word:
            return N.POSSIBLE_EPITHET
        if N.first_cap.match(word):
            return N.POSSIBLE_TAXON
    return N.UNKNOWN_TAG

def _process_after(name_map, word_list, split_ind, include_suffix):
    host = word_list[split_ind + 1:]
    host_name = ' '.join(host)
    if host_name:
        added, host_nd, suff = classify_name(name_map, host_name, include_suffix)
        return host_nd, suff
    return False, ''

def _process_before(name_map, word_list, split_ind):
    associate = word_list[:split_ind]
    assoc_name = ' '.join(associate)
    if assoc_name:
        added, assoc_nd, suff = classify_name(name_map, ' '.join(associate))
        if assoc_nd is True:
            return N(associate, N.KNOWN_TAXON)
        return assoc_nd
    return False
    
def _classify_split(name_map, word_list, split_ind, include_suffix):
    host_n, suff = _process_after(name_map, word_list, split_ind, include_suffix)
    if not host_n or (not host_n.has_taxonomic_name()):
        return (False, False, suff)
    assoc_n = _process_before(name_map, word_list, split_ind)
    if not assoc_n or (not assoc_n.has_taxonomic_name()):
        return (host_n, False, suff)
    return (host_n, assoc_n, suff)

def poorlyFormed(name):
    return True, N(name, N.POORLY_FORMED), ''
    
class HomonymError(Exception):
    def __init__(self, name, match_list):
        self.name = name
        self.matches = list(match_list)
    def __str__(self):
        s = []
        for m in self.matches:
            try:
                s.append('"%s" (%d)' % (m.original_name, m.ottol_id))
            except:
                s.append('"%s"' % m.original_name)
        return 'Homonym "%s" matches: %s' % (self.name, ', '.join(s))

def get_name_obj_from_map(name_map, name):
    obj = name_map.get(name)
    if obj is not None:
        if isinstance(obj, N):
            return obj
        if len(obj) > 1:
            name_obj_list = [name_map.get((name, i)) for i in obj]
            raise HomonymError(name, name_obj_list)
        else:
            assert len(obj) == 1
        obj = name_map.get((name, obj[0]))
    return obj
    
def classify_name(name_map, name, stack=0, include_suffix=True):
    '''
    Returns blob where the blob is `True` in `name` occurs the `name_map`
    and `blob` is 
    '''
    assert (name)
    # Any recognized taxon is just returned as True, may want to create N KNOWN_TAXON obj.
    obj = get_name_obj_from_map(name_map, name)
    if obj is not None:
        return False, obj, ''
    # Unrecognized name
    return classify_name_word_list(name_map, name.split(), stack, include_suffix)

def classify_name_word_list(name_map, word_list, stack=0, include_suffix=True):
    lwl = len(word_list)
    name = ' '.join(word_list)
    # Single word -> Classify term type and exit
    if lwl == 1:
        obj = obj = get_name_obj_from_map(name_map, word_list[0])
        if obj is not None:
            return False, obj, ''
        first_word_type = classify_word_type(word_list[0])
        return True, N(name, first_word_type, rank=TR.UNKNOWN), ''

    # categorize the type of word in each position. HACKY
    code_list = [classify_word_type(i) for i in word_list]
    
    # We can't do much with a string with a first word that does not look like a name...
    if code_list[0] != N.POSSIBLE_TAXON:
        _LOG.debug('"%s" does not start with Genus name' % name)
        return True, N(name, N.PERPLEXING), ''

    ng, genus_name, suffix = classify_name_word_list(name_map, [word_list[0]])
    
    if N.EX_TAG in code_list:
        if (not include_suffix) or code_list.count(N.EX_TAG) > 1:
            _LOG.debug('ex in odd context in "%s"' % name)
            return poorlyFormed(name)
        ex_ind = code_list.index(N.EX_TAG)
        host, assoc, suff = _classify_split(name_map, word_list, ex_ind, False)
        if host is False or assoc is False:
            _LOG.debug('pair of names around ex not parsed in "%s"' % name)
            return poorlyFormed(name)
        else:
            return True, N(name, N.EX_TAG, sub=(assoc, host), par=genus_name), suff

    # If we see the "var" syntax in the appropriate context, parse parent and augment
    if N.VAR_TAG in code_list:
        var_ind = code_list.index(N.VAR_TAG)
        pos_ok = bool(code_list.count(N.VAR_TAG) == 1 and var_ind > 1 and (var_ind + 1) < lwl)
        if pos_ok and code_list[var_ind + 1] == N.POSSIBLE_EPITHET:
            par = _process_before(name_map, word_list, var_ind)
            if include_suffix:
                suffix, suffix_type = _process_trailing(name_map, word_list, var_ind + 2)
                return True, N(name, 
                         N.VARIETY_OF,
                         par=par, 
                         rank=TR.VARIETY, 
                         entity_id_list=suffix, 
                         id_type=suffix_type), ''
            else:
                return True, N(name, 
                         N.VARIETY_OF,
                         par=par, 
                         rank=TR.VARIETY), ' '.join(word_list[var_ind + 2:])
        else:
            _LOG.debug('var in odd context in "%s"' % name)
            return poorlyFormed(name)


    # If we see the "subspecies" syntax in the appropriate context, it can be parsed by omitting the word..
    if N.SSP_TAG in code_list:
        if code_list.count(N.SSP_TAG) == 1: 
            ssp_ind = code_list.index(N.SSP_TAG)
            if ssp_ind > 1 and ssp_ind + 1 < lwl:
                without_ssp = word_list[:ssp_ind] + word_list[(ssp_ind + 1):]
                added, parsed_without_ssp, suff = classify_name_word_list(name_map, without_ssp, stack, include_suffix)
                if not parsed_without_ssp.is_subspecies():
                    _LOG.debug('dropping ssp tag leads to name that is not a subsp "%s"' % name)
                    parsed_without_ssp.flag_as_poorly_formed()
                parsed_without_ssp.original_name = name
                parsed_without_ssp.add_name_diff(N.SSP_TAG)
                return True, parsed_without_ssp, suff
        else:
            _LOG.debug('multiple ssp in "%s"' % name)
            return poorlyFormed(name)

    # affinity to species or subspecies...
    if N.AFF_TAG in code_list:
        if code_list.count(N.AFF_TAG) > 1:
            _LOG.debug('multiple aff tags in in "%s"' % name)
            return poorlyFormed(name)
        aff_ind = code_list.index(N.AFF_TAG)
        if (aff_ind > 0 and aff_ind + 1 < lwl) or (not include_suffix):
            without_aff = word_list[:aff_ind] + word_list[(aff_ind + 1):]
            added, parsed_without_aff, suff = classify_name_word_list(name_map, without_aff, stack, include_suffix=False)
            if parsed_without_aff.is_species():
                e = N.AFFINITY_TO_SPECIES
            elif parsed_without_aff.is_subspecies():
                e = N.AFFINITY_TO_SPECIES
            else:
                _LOG.debug('aff tag does not appear to apply to sp or ssp "%s"' % name)
                return poorlyFormed(name)
            if suff:
                sl, cl = _process_trailing(name_map, suff.split(' '), 0)
            else:
                sl, cl = None, None
            x = parsed_without_aff.canonical_name.split()
            if aff_ind + 1 >= len(x):
                cn = name
                _LOG.debug('aff followed by nothing in "%s"' % name)
                e = N.POORLY_FORMED
            else:
                cn = ' '.join(x[:aff_ind] + ['aff'] + x[aff_ind + 1:])
            aff_name = N(name, 
                            e,
                            par=parsed_without_aff.par,
                            sub=parsed_without_aff,
                            rank=parsed_without_aff.rank,
                            entity_id_list=sl,
                            id_type=cl,
                            canonical=cn)
            return True, aff_name, ''
        else:
            _LOG.debug('aff in odd context in "%s"' % name)
            return poorlyFormed(name)

    # deal with Genus sp or Genus sp nov
    if N.SP_TAG in code_list:
        if code_list.count(N.SP_TAG) == 1:
            sp_ind = code_list.index(N.SP_TAG)
            if sp_ind == 0:
                _LOG.debug('sp starting string in "%s"' % name)
                return poorlyFormed(name)
            if sp_ind + 1 == lwl:
                return True, N(name, N.GENUS_SP, par=genus_name, rank=TR.SPECIES), ''
            else:
                next_word = word_list[sp_ind + 1]
                if (next_word in ['nov', 'nov.', 'novum', 'nova']) or N.novum_id.match(next_word):
                    if N.novum_id.match(next_word):
                        # include nov1, nov2... in the suffix
                        sl, cl = _process_trailing(name_map, word_list[sp_ind + 1:], 0)
                    else:
                        if sp_ind + 2 < lwl:
                            sl, cl = _process_trailing(name_map, word_list[sp_ind + 2:], 0)
                        else:
                            sl, cl = _process_trailing(name_map, word_list[sp_ind + 2:], 0)
                    return (True, N(name, 
                                   N.GENUS_SP_NOV,
                                   par=genus_name,
                                   rank=TR.SPECIES,
                                   entity_id_list=sl,
                                   id_type=cl), '')
                else:
                    sl, cl = _process_trailing(name_map, word_list[sp_ind + 1:], 0)
                return (True, N(name,
                               N.GENUS_SP,
                               par=genus_name,
                               rank=TR.SPECIES,
                               entity_id_list=sl,
                               id_type=cl), '')
        else:
            _LOG.debug('multiple sp tags "%s"' % name)
            return poorlyFormed(name)
    if code_list[1] == N.POSSIBLE_EPITHET:
        if lwl == 2:
            return True, N(name, N.BINOMEN, par=genus_name, rank=TR.SPECIES), ''
        if code_list[2] == N.POSSIBLE_EPITHET:
            ng, sp_name, suffix = classify_name_word_list(name_map, word_list[:2])
            if lwl == 3:
                return True, N(name, N.TRINOMEN, par=sp_name, rank=TR.SUBSPECIES), ''
            sl, cl = _process_trailing(name_map, word_list, 3)
            return (True, N(name,
                            N.TRINOMEN,
                            par=sp_name,
                            rank=TR.SUBSPECIES,
                            entity_id_list=sl,
                            id_type=cl), '')
        else:
            sl, cl = _process_trailing(name_map, word_list, 2)
            return (True, N(name,
                            N.BINOMEN,
                            par=genus_name,
                            rank=TR.SPECIES,
                            entity_id_list=sl,
                            id_type=cl), '')
    m = N.epithet_with_suffix.match(word_list[1])
    if m:
        epi, suff = m.groups()
        split_name = [word_list[0], epi] + [suff] +  word_list[2:]
        r, n, s = classify_name_word_list(name_map, split_name, stack=stack+1, include_suffix=include_suffix)
        n.original_name = name
        return r, n, s
    _LOG.debug('Second word in "%s" is not an epithet' % name)
    return True, N(name, N.PERPLEXING), ''


def name_triple_to_obj_map(name2ids, return_root=False):
    name_map = {}
    id2o = {}
    max_id = 0
    for name_id_pair, v in name2ids.iteritems():
        if isinstance(name_id_pair, tuple):
            k, name_id = name_id_pair
            o = N(k, N.PREPROCESSED, ottol_id=v[0], par=v[1])
            max_id = max(max_id, max(v))
            name_map[name_id_pair] = o
            id2o[v[0]] = o
        else:
            name_map[name_id_pair] = v
    name2ids.clear()
    if return_root:
        root_nd = None
        for k, v in name_map.iteritems():
            if isinstance(v, N):
                if v.par < 1:
                    root_nd = v
                    v.par = None
                else:
                    p = id2o.get(v.par)
                    v.par = p
                    if p is not None:
                        if 'child_list' in p.__dict__:
                            p.child_list.append(v)
                        else:
                            p.child_list = [v]
    else:
        for k, v in name_map.iteritems():
            if isinstance(v, N):
                v.par = id2o.get(v.par)
    id2o.clear()
    if return_root:
        return name_map, max_id, root_nd
    return name_map, max_id

def triple_filename_to_name2obj(fn, return_root=False):
    name2ids = taxon_file_to_dict(open(fn, 'rU'))
    return name_triple_to_obj_map(name2ids, return_root)
    

class Taxonomy(object):
    def __init__(self, triple_filename, fast_parse=False):
        if fast_parse:
            name_map, max_id = triple_filename_to_name2obj(triple_filename)
            root = None
        else:
            name_map, max_id, root = triple_filename_to_name2obj(triple_filename, True)
        self.root = root
        self.max_id = max_id
        self.name_map = name_map

    def classify_name(self, name, add_to_taxonomy=False, addition_out_stream=None):
        was_matched, blob, suffix = classify_name(self.name_map, name)
        without_id = blob.get_attached_lacking_id()
        if add_to_taxonomy:
            for i in without_id:
                self.max_id += 1
                i.ottol_id = self.max_id
                name2idlist = self.name_map.get(i.canonical_name)
                if name2idlist is None:
                    name2idlist = []
                    self.name_map[i.canonical_name] = name2idlist
                name2idlist.append(i.ottol_id)
                self.name_map[(i.canonical_name, i.ottol_id)] = i
            if addition_out_stream:
                for i in without_id:
                    if i.has_taxonomic_name():
                        if i.par and (i.par.ottol_id is not None):
                            addition_out_stream.write('%d,%d,%s\n' % (i.ottol_id, i.par.ottol_id, i.canonical_name))
                        else:
                            addition_out_stream.write('%d,,%s\n' % (i.ottol_id, i.canonical_name))
        return blob, without_id
    
        
if __name__ == '__main__':
    triple_taxon_filename = arg_list[1]
    taxonomy = Taxonomy(triple_taxon_filename, True)
    tl_fn = arg_list[2]
    taxa_list_fo = open(tl_fn, 'rU')
    num_found = 0
    for line in taxa_list_fo:
        ls = line.strip()
        try:
            name_obj, added = taxonomy.classify_name(ls, True)
            for i in added:
                sys.stderr.write('%s\n' % i.diagnostic_str())
            if len(added) == 0:
                num_found += 1
        except:
            sys.stderr.write('Choked on:\n%s' % line)
            raise
    sys.stderr.write('%d names matched\n' % num_found)
