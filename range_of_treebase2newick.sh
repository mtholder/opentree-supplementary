#!/bin/sh
# Script to use NCLconverter to convert TreeBase NEXUS files to newick
# Invocation:
#   sh range_of_treebase2newick.sh 10000 10100
# in a directory with filenames like S10001.nex to convert each file
# name like S10000.nex up to file S10100.nex to newick.
# you should get files like:
#   outS####.tre  newick tree for the first trees block
#   2outS####.tre , 3outS####.tre , etc if the file has multiple output blocks
#   outS####.NameTranslationFile.txt is an ad hoc xml description of the name mapping
#   S####.outerr.txt will be the standard out and standard err of the NCLconverter process.
#
# Failure on a conversion will not cause the script to quit, you should just
#   see a error message.
if test -z $1 -o -z $2
then
    echo expecting two range args
    exit 1
fi
prefix="S"
s=$1
e=$2
rm -f NameTranslationFile*
for ((i=$1 ; i <= $2 ; ++i))
do
    inp="${prefix}${i}.nex"
    if test -f "${inp}"
    then
        if NCLconverter -erelaxedphylip "${inp}" "-oout${prefix}${i}x" > "${prefix}${i}.outerr.txt" 2>&1
        then 
            mv NameTranslationFile "out${prefix}${i}.NameTranslationFile.txt" 
            rm -f *"out${prefix}${i}".*.phy
        else
            echo "Conversion of ${inp} to relaxed phylip failed! Command:"
            echo "   " NCLconverter -erelaxedphylip "${inp}" "-oout${prefix}${i}x"
        fi
        rm -f NameTranslationFile*
    fi
done
